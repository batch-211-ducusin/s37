/*

App: Booking System API

Scenario:
	A course booking system application where a user can enroll into a course

Type: Course Booking System (Web App)

Description: 
	- A course booking system application where a user can enroll into a course
	- Allows an admin to do CRUD operations
	- Allows users to register into our database.
	
Features:
	- User login (User Authentication)
	- User Registration
	
	Customer/Authenticated Users:
	- View Courses (all active courses)
	- Enroll Course

	Admin Users:
	- Add Course
	- Update Course
	- Archive/Unarchive a course (soft delete / reactivate the course)
	- View Courses (all Courses active/inactive)
	- View/Manage User Accounts**

	All Users (guest, Customers, Admin)
	- View Active Courses


*/

/*
Data Model for the Booking System
*/

//Two-way Embedding


// Embedding
/*
user{
	id - unique ID
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin,
	enrollments: [
		{
			id - document identifier
			courseID - the unique
			CourseName - optional
			isPaid
			dateEnrolled
		}
	]
}


course {
	id - unique for the document
	name,
	description,
	price,
	slots,
	isActive,
	createdOn,
	enrollees: [
		{
			id - document identifier
			userId,
			isPaid,
			dateEnrolled
		}
	]
}
*/



// With Referencing
/*
user{
	id - unique ID
	firstName,
	lastName,
	email,
	password,
	mobileNumber,
	isAdmin
}

course {
	id - unique for the document
	name,
	description,
	price,
	slots,
	isActive,
	createdOn
}

enrollment {
	id - unique for the document
	userId - the unique identifier for the user
	courseId - the unique identifier for the course
	courseName - optional
	isPaid,
	dateEnrolled.
}
*/

/*
Mini Activity

	1. Create a models folder and create a "Course.js" file to store the schema of our courses

	2. Put the necessary data types of our fields
*/

