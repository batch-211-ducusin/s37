//Booking System API-Business Use Case Translation to Model Design
/*
MVP
	- Minimum Viable Product


Booking System MVO requirements
	- Minimum Viable Product is a product that has enough features to be useful to its target market. It is used to validate a product idea at the onset of development.


Our Booking System MVP must have the following features:
	1. User registration
	2. User Authentication


Booking System API Dependencies
	Besides express and mongoose, we have the following additional packages:

	1. bcrypt - for hashing user passwords prior to storage.
	2. cors - for allowing cross-origin resource sharing.
	3. jsonwebtoken - for implementing JSON
*/
const express = require('express');
const mongoose = require('mongoose');

// Allows our backend application to be available to our frontend application
// Allows us to control the app's Cross Origin Resource Sharing settings
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const courseRoutes = require("./routes/courseRoutes");

const app = express();

mongoose.connect('mongodb+srv://admin123:admin123@cluster0.cxdyx16.mongodb.net/s37-41?retryWrites=true&w=majority', {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

mongoose.connection.once('open', ()=> console.log(`Now connected to MongoDB Atlas.`));


// Allows all resources to access our backend application
app.use(cors());


/*
	- Ang "express.json()" at "express.urlencoded()" middleware ay kailangan para sa POST and PUT request, dahil ang mga request na ito ay nag sesend ng data sa database at ang mga middleware na ito ang nag sasabi sa database na i-store ang data.

	- Pero, ang mga middleware na to ay hindi na kailangan para sa GET at DELETE operation.
*/
app.use(express.json());
app.use(express.urlencoded({extended:true}));

app.use('/users', userRoutes);
app.use("/courses", courseRoutes);

app.listen(process.env.PORT || 4000,()=> {
	console.log(`API is now online on port ${process.env.PORT || 4000}`)
});