const Course = require("../models/Course");
const auth = require("../auth");

//Mini Activity
//Create a new course
/*
	steps:
	1. Create a new Course object using the mongoose model and the information from the reqBody and the id from the header.
	2. Save the new User to the database.
*/

/*module.exports.addCourse = (reqBody)=> {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, err)=> {
		if(err){
			return false;
		} else {
			return true;
		}
	})
}*/


//s39 Activity
module.exports.addCourse = (reqBody)=> {
	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	return newCourse.save().then((course, error)=> {
		if(error){
			return false
		} else {
			return true
		}
	})
}


//Retrieve all courses
/*
	1. Retrieve all the courses from the database Model .find({})
*/
module.exports.getAllCourses = ()=> {
	return Course.find({}).then(result=> {
		return result;
	})
}

//Retrieve all active courses
/*
	1. Retrieve all the courses from the database with the property of "isActive" to true.
*/
module.exports.getAllActive = ()=> {
	return Course.find({isActive: true}).then(result=> {
		return result;
	})
};


//Retrieve a specific course.
/*
	1. retrieve the course that match the course ID provided from the url.
*/
module.exports.getCourse = (reqParams)=> {
	return Course.findById(reqParams.courseId).then(result=> {
		return result;
	});
};


//Controller for updating a course.
/*
	1. Create a variable "updateCourse" which will contain the information retrieved from the request body.
	2. Find and update the course using the course ID retrieve from the request params property and the variable "updatedCourse" containing the infromation from the request body.
*/
module.exports.updateCourse = (reqParams, reqBody)=> {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};
	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error)=> {
		if(error){
			return false;
		} else {
			return true;
		};
	});
};


// s40 Activity Update the status of the course make it isActive: false (archiving a course)(soft delete)
module.exports.archiveCourse = (reqParams, reqBody)=> {
	let courseStatus = {
		isActive: reqBody.isActive
	};
	return Course.findByIdAndUpdate(reqParams.courseId, courseStatus).then((course, error)=> {
		if(error){
			return false;
		} else {
			return Course.findById(reqParams.courseId).then(result=> {return result;});
		};
	});
};
