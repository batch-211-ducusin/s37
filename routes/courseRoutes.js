const express = require("express");
const router = express.Router();
const courseController = require("../controllers/courseControllers");
const auth = require("../auth");

//Route for creating a course
//refactor this course route to implement user authentication for our admin
/*router.post("/", auth.verify, (req, res)=> {

	const userData = auth.decode(req.headers.authorization)
	courseController.addCourse(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController=> res.send(resultFromController));
});*/


//s39 Activity
router.post("/", auth.verify, (req, res)=> {
	const userAdmin = auth.decode(req.headers.authorization).userAdmin
	if(userAdmin == true){
		courseController.addCourse(req.body).then(resultFromController=> res.send(resultFromController));
	} else {
		res.send("Authorization Failed!");
	}
})


//Route for retrieving all the courses.
router.get("/all", (req, res)=> {
	courseController.getAllCourses().then(resultFromController=> res.send(resultFromController));
});

//Route for retrieving all active courses.
router.get("/", (req, res)=> {
	courseController.getAllActive().then(resultFromController=> res.send(resultFromController))
});


//Route for retrieving a specific course.
router.get("/:courseId", (req, res)=> {
	courseController.getCourse(req.params).then(resultFromController=> res.send(resultFromController));
});


//Route for updating a course.
router.put("/:courseId", auth.verify, (req, res)=> {
	courseController.updateCourse(req.params, req.body).then(resultFromController=> res.send(resultFromController));
});


// s40 Activity Update the status of the course make it isActive: false (archiving a course)(soft delete)
router.put("/:courseId/archive", auth.verify, (req, res)=> {
	courseController.archiveCourse(req.params, req.body).then(resultFromController=> res.send(resultFromController));
});





module.exports = router; 